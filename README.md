<h1>sublime text setup</h1>
<h2>disclaimer: this is my personal setup and only working on windows!</h2>
<h4>Installed packages:</h4>
<ol>
	<li>Emmet</li>
	<li>Browser Sync</li>
	<li>MarkdownPreview</li>
</ol>

<details open>
<summary><b>sublime-build</b></summary>
<p>Create a new file with extension <b>file.sublime-build</b>.</p>

<table><tr><td>
<details open>
<summary>c++</summary>

```json
{
	"cmd":["g++.exe", "-o", "${file_base_name}.exe", "-std=c++17", "-O2", "-Wshadow", "${file}", "&&", "${file_base_name}.exe>output.txt"],
	"selector":"source.cpp,source.cxx,source.c++",
	"shell":true,
	"working_dir":"$file_path",
	"file_regex": "^(..[^:]*):([0-9]+):?([0-9]+)?:? (.*)$",
	"variants":
	[
		{
			"name":"CPP INPUT",
			"cmd":["g++.exe", "-o", "${file_base_name}.exe", "-std=c++17", "-O2", "-Wshadow", "${file}", "&&", "${file_base_name}.exe<input.txt>output.txt"],
		}	
	]
}
```
</td></tr></details>

<tr><td>
<details>
<summary>java</summary>

```json
{
	"cmd":["javac","${file}","&&","java","${file_base_name}>output.txt"],
	"selector":"source.java",
	"shell":true,
	"working_dir":"$file_path",
	"file_regex": "^(..[^:]*):([0-9]+):?([0-9]+)?:? (.*)$",
	"variants":
	[
		{
			"name":"JAVA INPUT",
			"cmd":["javac","${file}","&&","java","${file_base_name}<input.txt>output.txt"],
		}	
	]
}
```
</tr></td></details>

<tr><td>
<details>
<summary>python</summary>

```json
{
	"cmd":["python.exe", "${file}>output.txt"],
	"selector":"source.python",
	"shell":true,
	"working_dir":"$file_path",
	"file_regex": "^(..[^:]*):([0-9]+):?([0-9]+)?:? (.*)$",
	"variants":
	[
		{
			"name":"PYTHON INPUT",
			"cmd":["python.exe", "${file}<input.txt>output.txt"],
		}	
	]
}
```
</tr></tr></details></table>
</details>

---
<details>
<summary><b>sublime-snippet</b></summary>

<p>Create a new file with extension <b>file.sublime-snippet</b>.</p>

<table><tr><td>
<details open>
<summary>c++</summary>

```xml
<snippet>
	<content><![CDATA[
//Author: Wahyu Priambodo / TMJ 2B / 2207421048
#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#define size(x) (sizeof(x)/sizeof(x[0]))
using namespace std;

int main() {
	${2:}

	//system("pause");
	return 0;
}
]]></content>
	<!-- Optional: Set a tabTrigger to define how to trigger the snippet -->
	<tabTrigger>cpptemp</tabTrigger>
	<!-- Optional: Set a scope to limit where the snippet will trigger -->
	<scope>source.c++</scope>
</snippet>
```
</tr></td></details>

<tr><td><details>
<summary>java</summary>

```xml
<snippet>
	<content><![CDATA[
//Author: Wahyu Priambodo / TMJ 2B / 2207421048
import java.util.*;

public class ${TM_FILENAME/(.+)\..+|.*/$1/:name} {
	public static void main(String[] args) {
		${2:}
	}
}
]]></content>
	<!-- Optional: Set a tabTrigger to define how to trigger the snippet -->
	<tabTrigger>javatemp</tabTrigger>
	<!-- Optional: Set a scope to limit where the snippet will trigger -->
	<scope>source.java</scope>
</snippet>
```
</tr></td></details>

<tr><td><details>
<summary>python</summary>

```xml
<snippet>
	<content><![CDATA[
#Author: Wahyu Priambodo / TMJ 2B / 2207421048

def main() -> None:
	${2:}

if __name__ == "__main__":
	main()

]]></content>
	<!-- Optional: Set a tabTrigger to define how to trigger the snippet -->
	<tabTrigger>pytemp</tabTrigger>
	<!-- Optional: Set a scope to limit where the snippet will trigger -->
	<scope>source.python</scope>
</snippet>
```
</tr></td></details></table>
</details>